<?php

$dtz= new DateTimeZone('Europe/Kiev');
$currentDate=new DateTime('now',$dtz);
$currentDate1=new DateTime('now',$dtz);


$todayStartDate =$currentDate -> modify('today midnight');
$todayEndDate=$currentDate1 -> modify('tomorrow midnight -1 second');

echo "Сегодня:";
echo "<br>";
echo $todayStartDate->format('Y-m-d H:i:s');
echo "<br>";
echo $todayEndDate->format('Y-m-d H:i:s');
echo "<br>";



$yesterdayStartDate=$currentDate-> modify('yesterday midnight');
$yesterdayEndDate=$currentDate1-> modify('today midnight -1 second');


echo "<br>";
echo "Вчера:";
echo "<br>";
echo $yesterdayStartDate->format('Y-m-d H:i:s');
echo "<br>";
echo $yesterdayEndDate->format('Y-m-d H:i:s');
echo "<br>";



$thisWeekStartDate=$currentDate-> modify('this week monday midnight');
$thisWeekEndDate=$currentDate1-> modify('next week monday midnight -1 second');

echo "<br>";
echo "Текущая неделя:";
echo "<br>";
echo $thisWeekStartDate->format('Y-m-d H:i:s');
echo "<br>";
echo $thisWeekEndDate->format('Y-m-d H:i:s');
echo "<br>";



$lastWeekStartDate=$currentDate-> modify('last week monday midnight');
$lastWeekEndDate=$currentDate1-> modify('this week monday midnight -1 second');

echo "<br>";
echo "Прошлая неделя:";
echo "<br>";
echo $lastWeekStartDate->format('Y-m-d H:i:s');
echo "<br>";
echo $lastWeekEndDate->format('Y-m-d H:i:s');
echo "<br>";


$thisMonthStartDate=$currentDate-> modify('first day of this month midnight');
$thisMonthEndDate=$currentDate1-> modify('first day of next month midnight - 1 second');

echo "<br>";
echo "Текущий месяц:";
echo "<br>";
echo $thisMonthStartDate->format('Y-m-d H:i:s');
echo "<br>";
echo $thisMonthEndDate->format('Y-m-d H:i:s');
echo "<br>";


$lastMonthStartDate=$currentDate-> modify('first day of last month midnight');
$lastMonthEndDate=$currentDate1-> modify('first day of this month midnight - 1 second');

echo "<br>";
echo "Прошлый месяц:";
echo "<br>";
echo $lastMonthStartDate->format('Y-m-d H:i:s');
echo "<br>";
echo $lastMonthEndDate->format('Y-m-d H:i:s');
echo "<br>";


$thisYearStartDate=$currentDate-> modify('first day of January this year midnight');
$thisYearEndDate=$currentDate1-> modify('first day of January next year midnight - 1 second');

echo "<br>";
echo "Текущий год:";
echo "<br>";
echo $thisYearStartDate->format('Y-m-d H:i:s');
echo "<br>";
echo $thisYearEndDate->format('Y-m-d H:i:s');
echo "<br>";


$lastYearStartDate =$currentDate-> modify('first day of January last year midnight');
$lastYearEndDate=$currentDate1-> modify('first day of January this year midnight - 1 second');

echo "<br>";
echo "Прошлый год:";
echo "<br>";
echo $lastYearStartDate->format('Y-m-d H:i:s');
echo "<br>";
echo $lastYearEndDate->format('Y-m-d H:i:s');
echo "<br>";