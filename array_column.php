<?php
$records = array(
    array(
        'id' => 2135,
        'first_name' => 'John',
        'last_name' => 'Doe',
    ),
    array(
        'id' => 3245,
        'first_name' => 'Sally',
        'last_name' => 'Smith',
    ),
    array(
        'id' => 5342,
        'first_name' => 'Jane',
        'last_name' => 'Jones',
    ),
    array(
        'id' => 5623,
        'first_name' => 'Peter',
        'last_name' => 'Doe',
    )
);

function my_array_column(array $array, string $column_key, string $index_key = null): array {

    $newArray=[];
    foreach ($array as $key =>$value){
        if (isset($value[$column_key])){
            if ($index_key!= null){
                $key=$value[$index_key];
            }
            $newArray[$key]=$value[$column_key];
        }
    }
    return $newArray;

};

echo "<pre>";
var_dump(my_array_column($records,'first_name','id' ));
echo "<pre>";