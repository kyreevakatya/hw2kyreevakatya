<?php
$a = array(2, 4, 6, 8);

function my_array_sum(array $array) {
         $sum = null;
         if (count($array)==0){
             $sum=0;
         }
         foreach($array as $value){
             $sum+=$value;
         }
         return $sum;
}

echo my_array_sum($a);