<?php

$os = array("Mac", "NT", "Irix", "Linux");

function my_in_array ( $needle, array $haystack, bool $strict = false): bool
{
    for ($i = 0, $c = count($haystack); $i < $c; $i++) {
        if ($strict = false) {
            if ($needle == $haystack[$i]) {
                return true;
            }
        } elseif ($strict = true) {
            if ($needle === $haystack[$i]) {
                return true;
            }
        }
    }return false;
}


if (my_in_array("Irix", $os)) {
    echo "Нашёл значение";
} else {
    echo "Не нашел значение";
}